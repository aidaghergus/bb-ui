import React, { Component } from "react";
import {Avatar, Button, Col, Dropdown, Layout, Menu, notification, Row} from "antd";
import { Link } from "react-router-dom";
import './Header.less'
import {EventEmitter} from "fbemitter";
import UserStore from "../apiserver/UserStore";
import { DownOutlined } from "@ant-design/icons";
import { Typography } from 'antd';

const { Text } = Typography;


const ee = new EventEmitter();
const userStore = new UserStore(ee);



class Header extends Component {

  UserProfile(props, state) {
    return (
        <Row>
          <Col span={10} style={{textAlign:"center", marginTop:"9px"}}>
          <Text type={"secondary"} style={{color:"rgba(255, 255, 255, 0.65)"}}>{state.user.username}</Text>
          </Col>
          <Col span={[14]}>
          <Dropdown
              trigger={['hover','click']}
              overlay={
                <Menu>

                  <Menu.Item key="profile" onClick={() =>{
                      window.location.href="/profile"
                  }}>>
                    Profile

                  </Menu.Item>
                  <Menu.Item key="logout" onClick={() =>{
                    localStorage.removeItem("Credentials");
                    window.location.href="/home"
                  }}>
                    Logout
                  </Menu.Item>
                </Menu>
              }
          >
            <div>
              <a href={"/profile"}>
                <Avatar className="user-avatar user-avatar-mini" style={{marginTop:"5px"}}
                        >{state.user.firstName.substring(0,1)+state.user.lastName.substring(0,1)}</Avatar>
              </a>
              <DownOutlined style={{ marginLeft: "20px" }} />
            </div>
          </Dropdown>
          </Col>
        </Row>
    );
  }

  constructor(props){
    super(props);
    this.state={
      token:null,
      user:null
    }
  }

  componentDidMount() {
    let credentials;
    let serializedCredentials=localStorage.getItem("Credentials");
    if(serializedCredentials!=null){
      credentials=JSON.parse(serializedCredentials);
      userStore.validateToken(credentials.token);
    }

    ee.addListener('TOKEN_VALIDATED', ()=>{
      this.setState({
        token:credentials.token,
        user:credentials.user
      });
    });

    ee.addListener('TOKEN_INVALIDATED', ()=>{
      localStorage.removeItem("Credentials");
      this.setState({
        token:null
      });

    })

  }


  render() {
    return (
      <Row
        style={{
          position: "fixed",
          zIndex: 1,
          width: "100%",
          backgroundColor:"#001529",
          textAlign:"center"
        }}
      >
        <Col span={6}>
        <Menu theme="dark" mode="horizontal">
          <Menu.Item key="1" style={{ alignSelf: "left" }}>
            <Link to="/">
              <div className="logo">
                <img
                  src="https://i.ibb.co/n8ktrsV/logo2.png"
                  style={{ width: "25px", height: "25px" }}
                />
              </div>
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <a href="/about">About</a>
          </Menu.Item>
          <Menu.Item key="2"><Link to={"/barterplace"}>BarterPlace</Link></Menu.Item>
        </Menu>
      </Col>
        <Col span={2} offset={16}>
          {!this.state.token ?
              <Button type={"primary"} className={"log-in"}><Link to={"/login"}>Log in</Link></Button>
              : this.UserProfile(this.props, this.state)}
        </Col>

      </Row>
    );
  }
}

export default Header;
