import React, {Component} from 'react'
import {Steps, Button, message, Typography} from 'antd';
import './AboutUs.less'

const { Step } = Steps;
const { Title } = Typography;


const steps = [
    {
        title: 'First',
        content: <div>
            <p>First you need to make an account and post at least one item.</p><br/>
            <p>Feel free to add photos, in this way it is easier to barter.</p><br/>
            <p>You will also need an Etherum account as BarterBlock is a decentralized application.</p>
        </div>
    },
    {
        title: 'Second',
        content:<div>
            <p>Second, explore Barterplace and search for an item. When there is something you like, hit make offer.</p><br/>
            <p>When you offer a product, you sign an agreement so be sure to have Metamask enabeled.</p><br/>
            <p>After you made an offer you can watch it accessing Profile; in this section you can also see if someone made you an offer.</p><br/>
            <p>When products are included in an offer, their status will be on WAITING until the agreement is accepted or not.<br/> Offers can be made only for and including an AVAILABLE product.</p>
        </div>
    },
    {
        title: 'Last',
        content: <div>
            <p>A barter is completed when two users acccept the transaction.</p><br/>
            <p>Any received offer can be accepted (signed) or denied. The completed barters can be seen also in Profile section -> My Barters. </p><br/>
            <p>For any completed transaction you can see its signatures stored in blockchain. Once the barter is completed, the items' status gets on TRADED.</p>
            <br/><a href={"/barterplace"}>HOPE YOU ENJOY! Let's barter!</a>
        </div>,
    },
];

class AboutUs extends Component{
    constructor(props){
        super(props);
        this.state = {
            current: 0,
        };
    }

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    render() {
        const { current } = this.state;
        return(<div style={{margin:"0px 100px 100px 100px", padding: "80px"}}>
                <div style={{padding:"20px", textAlign:"center"}}><Title level={3}>How to complete a barter transaction?</Title></div>
                <Steps current={current}>
                    {steps.map(item => (
                        <Step key={item.title} title={item.title} />
                    ))}
                </Steps>
                <div className="steps-content">{steps[current].content}</div>
                <div className="steps-action">
                    {current < steps.length - 1 && (
                        <Button type="primary" onClick={() => this.next()}>
                            Next
                        </Button>
                    )}

                    {current > 0 && (
                        <Button style={{ margin: '0 8px' }} onClick={() => this.prev()}>
                            Previous
                        </Button>
                    )}
                </div>
            </div>
        )

    }

}

export default AboutUs;
