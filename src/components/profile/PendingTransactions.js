import React, {Component} from 'react';
import {EventEmitter} from "fbemitter";
import AgreementStore from "../../apiserver/AgreementStore";
import {Alert, Tag, Typography, Table, Popconfirm, Button, Spin, notification} from "antd";
import {Link} from "react-router-dom";
import Text from "antd/es/typography/Text";
import { QuestionCircleOutlined } from '@ant-design/icons';
import AddressStore from "../../apiserver/AddressStore";

const { Title } = Typography;
const ee = new EventEmitter();
const agreementStore = new AgreementStore(ee);
const userStore = new AgreementStore(ee);
const itemStore = new AgreementStore(ee);
const addressStore = new AddressStore(ee);


class PendingTransactions extends Component{
    constructor(props){
        super(props);
        this.state={
            pendingAgreements:[],
            waitingAgreements:[],
            loading:false
        }
    }

    componentDidMount() {

        if(this.props.token!=null){
            agreementStore.getPendingAgreements(this.props.token);

            ee.addListener('PENDING_AGREEMENTS_LOADED', ()=>{
                this.setState({
                    pendingAgreements: agreementStore.content,
                    loading:false
                });
                agreementStore.getWaitingAgreements(this.props.token)
            });

            ee.addListener('WAITING_AGREEMENTS_LOADED', ()=>{
                this.setState({
                    waitingAgreements: agreementStore.content,
                })

            })

        }

    }


    signAgreement(agreementId){
        this.setState({
            loading:true
        });

            addressStore.postAddress(this.props.token, this.props.userAcc.toString());

            ee.addListener('ADD_ADDRESS', () => {
                this.props.swapSign.methods.signAgreement(`0x${agreementId.toString(16)}`).send({from: this.props.userAcc})
                    .then((res) => {
                        agreementStore.signAgreement(this.props.token,agreementId);
                        agreementStore.getPendingAgreements(this.props.token);

                        notification.success({message: "You successfully barter a product"});

                    })
            });
    }

    denyAgreement(agreementId){
        this.setState({
            loading:true
        });

        agreementStore.denyAgreement(this.props.token,agreementId);
        ee.addListener('DENY_AGREEMENT', () => {

            agreementStore.getPendingAgreements(this.props.token);
            notification.success({message: "You barter offer was denied"});
        });

    }



    render() {
        return(
            <div>
                <div style={{lineHeight:"30px", paddingBottom:"15px", paddingTop:"15px"}}>

                <Text underline>
                    Solicitee Agreements
                </Text>

                </div>
                {
                    this.state.pendingAgreements.length===0 ?  <Alert
                        message="No pending solicitee agreements"
                        description="Nobody made offers for your products"
                        type="info"
                        showIcon
                    />: <div>

                    <Table columns={[
                        {
                            title: 'Agreement id',
                            dataIndex: 'id',
                            key: 'idSolicitee',
                        },
                        {
                            title: 'Product to be exchanged',
                            dataIndex: ['soliciteeOffer','itemDTO','name'],
                            key: 'productexchSolicitee',
                            render: (text,record) => <Link to={{pathname: "/item",
                                search: `?id=${record.soliciteeOffer.itemDTO.id}`,
                                state: { item: record.soliciteeOffer.itemDTO }}}>{text}</Link>,

                        },
                        {
                            title: 'Product offered',
                            dataIndex: ['solicitorOffer','itemDTO','name'],
                            key: 'productofferedSolicitee',
                            render: (text,record) => <Link to={{pathname: "/item",
                                search: `?id=${record.solicitorOffer.itemDTO.id}`,
                                state: { item: record.solicitorOffer.itemDTO }}}>{text}</Link>,
                        },
                        {
                            title: 'Solicitor\'s message',
                            dataIndex: ['solicitorOffer','offerMessage'],
                            key: 'messageSolicitee',
                        },

                        {
                            title: 'Status',
                            key: 'statusSolicitee',
                            dataIndex: 'status',
                            render: tag => (
                                <>
                                    <Tag color={"warning"}>
                                        {tag.toUpperCase()}
                                    </Tag>
                                </>
                            ),
                        } ,
                        {
                            title: 'Date',
                            dataIndex: 'date',
                            key: 'dateSolicitee',
                        },
                        {
                            title: 'Action',
                            key: 'action',
                            render: (text, record) => (
                                <div>
                                    <Popconfirm title="Are you sure? Once you accept the transaction is completed." onConfirm={() => {this.signAgreement(record.id)}} icon={<QuestionCircleOutlined style={{ color: 'red' }}/>}>
                                        <Button type={"link"} >Accept</Button>
                                    </Popconfirm>
                                    <Popconfirm title="Are you sure? One you deny, the offer in revoked." onConfirm={() => {this.denyAgreement(record.id)}} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                                        <Button type={"link"}>Deny</Button>
                                    </Popconfirm>
                                </div>
                            ),
                        },
                    ]} dataSource={this.state.pendingAgreements} />
                    </div>
                }

                <div style={{lineHeight:"30px", paddingBottom:"15px", paddingTop:"15px"}}>
                <Text underline >
                    Solicitor Agreements
                </Text>
                </div>

                {
                    this.state.waitingAgreements.length===0 ? <Alert
                        message="No pending solicitor agreements"
                        description="There is no pending offer that you made"
                        type="info"
                        showIcon
                    />:<div>
                        <Table columns={ [
                            {
                                title: 'Agreement id',
                                dataIndex: 'id',
                                key: 'idSolicitor',
                            },
                            {
                                title: 'Product you offered',
                                dataIndex: ['solicitorOffer','itemDTO','name'],
                                key: 'productofferedSolicitor',
                                render: (text,record) => <Link to={{pathname: "/item",
                                    search: `?id=${record.solicitorOffer.itemDTO.id}`,
                                    state: { item: record.solicitorOffer.itemDTO }}}>{text}</Link>,

                            },
                            {
                                title: 'Product in exchange',
                                dataIndex: ['soliciteeOffer','itemDTO','name'],
                                key: 'productexchSolicitor',
                                render: (text,record) => <Link to={{pathname: "/item",
                                    search: `?id=${record.soliciteeOffer.itemDTO.id}`,
                                    state: { item: record.soliciteeOffer.itemDTO }}}>{text}</Link>,

                            },
                            {
                                title: 'Your message',
                                dataIndex: ['solicitorOffer','offerMessage'],
                                key: 'messageSolicitor',
                            },

                            {
                                title: 'Status',
                                key: 'statusSolicitor',
                                dataIndex: 'status',
                                render: tag => (
                                    <>
                                        <Tag color={"warning"}>
                                            {tag.toUpperCase()}
                                        </Tag>
                                    </>
                                ),
                            } ,
                            {
                                title: 'Date',
                                dataIndex: 'date',
                                key: 'dateSolicitor',
                            }
                        ]} dataSource={this.state.waitingAgreements} />
                    </div>
                }

            </div>
        )
    }
}

export default PendingTransactions;
