import React, {Component} from 'react';
import {Avatar, Col, Row, Tabs, Card} from "antd";
import MyWallet from "../MyWallet";
import MyProducts from "./MyProducts";
import PendingTransactions from "./PendingTransactions";
import getWeb3 from "../../web3/getWeb3";
import SwapSign from "../../contracts/SwapSign";
import MyAgreements from "./MyAgreements";

const { TabPane } = Tabs;
const { Meta } = Card;

class Profile extends Component{
    constructor(props){
        super(props);
        this.state={
            user:null,
            token: null
        }
    }

    componentDidMount = async () => {

        //window.location.reload();
        let credentials;
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            credentials=JSON.parse(serializedCredentials);
            this.setState({
                user:credentials.user,
                token: credentials.token
            });
        }
        try {
            // Get network provider and web3 instance.
            const web3 = await getWeb3();

            // Use web3 to get the user's accounts.
            const accounts = await web3.eth.getAccounts();

            // Get the contract instance.
            const networkId = await web3.eth.net.getId();

            window.web3 = web3;
            window.user = (await web3.eth.getAccounts())[0];
            const balanceInWei = await web3.eth.getBalance(window.user);
            var balance = web3.utils.fromWei(balanceInWei, "ether");
            const deployedNetwork = SwapSign.networks[networkId];
            const instance = new web3.eth.Contract(
                SwapSign.abi,
                deployedNetwork && deployedNetwork.address
            );

            const networkType = await web3.eth.net.getNetworkType();


            this.setState({
                hasData: true,
                web3: web3,
                accounts: accounts,
                swapSign: instance,
                userAcc: window.user,
                balance: balance,
                networkId: networkId,
                networkType: networkType,
            });
        } catch (error) {
            alert(
                `Failed to load web3, accounts, or contract. Check console for details.`
            );
            console.error(error);
        }
    };


    render() {
        return(
            <div style={{ padding: "6vw" }}>
                {this.state.user && <Row gutter={{ xs: 8, sm: 16, md: 24 }}>
                    <Col span={5}>
                        <Card className={"user-card"}>
                            <Avatar className="user-avatar user-avatar-big user-avatar-shadow" shape={"circle"} id="user-card-avatar">
                                {this.state.user.firstName.substring(0,1)+this.state.user.lastName.substring(0,1)}
                            </Avatar>
                            <Meta
                                title={this.state.user.firstName +" "+ this.state.user.lastName}
                                description={
                                    <div>
                                        {this.state.user.username} <br />
                                        {this.state.user.email} <br />
                                        {this.state.user.phone} <br />
                                    </div>
                                }
                            />
                        </Card>
                    </Col>
                    <Col span={19}>
                        <Tabs  defaultActiveKey={"2"} >
                                <TabPane tab={"My Products"} key={"1"}>
                                    <MyProducts userId={this.state.user.id}/>
                                </TabPane>
                            <TabPane tab={"Pending Transactions"} key={"2"}>
                                <PendingTransactions swapSign={this.state.swapSign} userAcc={this.state.userAcc} token={this.state.token}/>
                            </TabPane>
                            <TabPane tab={"My Barters"} key={"3"}>
                                <MyAgreements swapSign={this.state.swapSign} userAcc={this.state.userAcc} token={this.state.token}/>
                            </TabPane>
                            <TabPane tab={"My Wallet"} key={"4"}>
                                <MyWallet hasData={this.state.hasData} user={this.state.userAcc} balance={this.state.balance} networkId={this.state.networkId} networkType={this.state.networkType} />
                            </TabPane>

                        </Tabs>
                    </Col>
                </Row>}
            </div>
        )
    }
}

export default Profile;
