import React, {Component} from 'react';
import {Button, Col, List, Row, Tag} from "antd";
import {
    DeleteTwoTone,
    FormOutlined,
    QuestionOutlined,
} from "@ant-design/icons";
import {EventEmitter} from "fbemitter";
import ItemStore from "../../apiserver/ItemStore";
import {Link} from "react-router-dom";

const ee = new EventEmitter()
const itemStore = new ItemStore(ee);

class MyProducts extends Component{
    constructor(props){
        super(props);
        this.state={
            loadingProducts:true,
            products: []
        }

    }
    componentDidMount() {
        itemStore.getItemsOfUser(this.props.userId);
        ee.addListener('USER_ITEMS_LOAD', ()=>{
            this.setState({
                products: itemStore.content,
                loadingProducts:false
            })
        });

    }
    colorReturn(status){
        if(status==="AVAILABLE")
            return "cyan";
        if(status==="WAITING")
            return "yellow";
        if(status==="TRADED")
            return "red";
    }
    handleDeleteProduct = () =>{

    };


    render() {
        return(
            <List
                size="large"
                bordered
                loading={this.state.loadingProducts}
                dataSource={this.state.products}
                renderItem={(item) => (
                    <List.Item
                        actions={[
                            /*<KeyOutlined
                              key={item.id}
                              title={"Change password"}
                              color="yellow"
                            />,*/
                            <DeleteTwoTone
                                key={item.id}
                                title={"Delete Product"}
                                style={{ fontSize: "18px" }}
                                twoToneColor="#eb2f96"
                                onClick={() => this.handleDeleteProduct(item.id)}
                            />,
                        ]}
                    >
                        <Link to={ {pathname: "/item",
                            search: `?id=${item.id}`,
                            state: { item: item }}}><List.Item.Meta title={item.name} description={item.description} /></Link>

                                <Tag color={this.colorReturn(item.status)}>
                                    {item.status}
                                </Tag>
                    </List.Item>
                )}
            />
        )
    }
}

export default MyProducts;
