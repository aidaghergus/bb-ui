import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Alert, Button, Popconfirm, Table, Tag} from "antd";
import {EventEmitter} from "fbemitter";
import AgreementStore from "../../apiserver/AgreementStore";
import Text from "antd/es/typography/Text";

const ee = new EventEmitter();
const agreementStore = new AgreementStore(ee);

class MyAgreements extends Component{
    constructor(props){
        super(props);
        this.state={
            accepted:[],
            denied:[]
        }


    }

    componentDidMount() {
        agreementStore.getAcceptedAgreements(this.props.token);
        ee.addListener('ACCEPTED_AGREEMENTS_LOADED', () => {
            let accepted=[...agreementStore.content];
            accepted=accepted.filter(el=>{
                if(el.status==='FINISHED')
                    return el;
            });
            agreementStore.getDeniedAgreements(this.props.token);
            ee.addListener('DENIED_AGREEMENTS_LOADED', () => {
                let denied = agreementStore.content;
                denied=denied.filter(el=>{
                    if(el.status==='DENIED')
                        return el;
                });

                this.setState({
                    accepted:accepted,
                    denied: denied
                })
            })



        })
    }

    render() {
        return(
            <div>

                <div style={{lineHeight:"30px", paddingBottom:"15px", paddingTop:"15px"}}>
                <Text underline>
                    Completed barters
                </Text>
                </div>
                {
                    this.state.accepted.length===0 ?  <Alert
                        message="No completed barters"
                        description="You did not make any transaction"
                        type="info"
                        showIcon
                    />: <div>


                <Table columns={[
                    {
                        title: 'Agreement id',
                        dataIndex: 'id',
                        key: 'idFin',
                    },
                    {
                        title: 'Description',
                        dataIndex: 'accord',
                        key: 'accordSolicitee',
                    },
                    {
                        title: 'Products exchanged',
                        dataIndex: ['soliciteeOffer','itemDTO','name'],
                        key: 'productSoliciteeFin',
                        render: (text,record) => {return (<ul>
                           <li> <Link to={{pathname: "/item",
                            search: `?id=${record.soliciteeOffer.itemDTO.id}`,
                               state: { item: record.soliciteeOffer.itemDTO }}}>{text}</Link> </li>
                    <li><Link to={{pathname: "/item",
                        search: `?id=${record.solicitorOffer.itemDTO.id}`,
                            state: { item: record.solicitorOffer.itemDTO }}}>{record.solicitorOffer.itemDTO.name}</Link></li></ul>)}

                    },

                    {
                        title: 'Status',
                        key: 'statusFin',
                        dataIndex: 'status',
                        render: tag => (
                            <>
                                <Tag color={"warning"}>
                                    {tag.toUpperCase()}
                                </Tag>
                            </>
                        ),
                    } ,
                    {
                        title: 'Date',
                        dataIndex: 'date',
                        key: 'dateSolicitee',
                    },
                    {
                        title: 'Action',
                        key: 'action',
                        render: (text, record) => (
                            <div>
                                    <Button type={"link"} >View signatures</Button>
                            </div>
                        ),
                    },
                ]} dataSource={this.state.accepted} />
                        </div>}



                <div style={{lineHeight:"30px", paddingBottom:"15px", paddingTop:"15px"}}>
                    <Text underline>
                        Denied barters
                    </Text>
                </div>

                {
                    this.state.denied.length===0 ?  <Alert
                        message="No denied barters"
                        description="You do not have any solicited and denied barter"
                        type="info"
                        showIcon
                    />: <div>

                        <Table columns={[
                            {
                                title: 'Agreement id',
                                dataIndex: 'id',
                                key: 'idDen',
                            },

                            {
                                title: 'Products to be exchanged',
                                dataIndex: ['soliciteeOffer','itemDTO','name'],
                                key: 'productDen',
                                render: (text,record) => {return (<ul>
                                    <li> <Link to={{pathname: "/item",
                                        search: `?id=${record.soliciteeOffer.itemDTO.id}`,
                                        state: { item: record.soliciteeOffer.itemDTO }}}>{text}</Link> </li>
                                    <li><Link to={{pathname: "/item",
                                        search: `?id=${record.solicitorOffer.itemDTO.id}`,
                                        state: { item: record.solicitorOffer.itemDTO }}}>{record.solicitorOffer.itemDTO.name}</Link></li></ul>)}

                            },

                            {
                                title: 'Status',
                                key: 'statusDen',
                                dataIndex: 'status',
                                render: tag => (
                                    <>
                                        <Tag color={"warning"}>
                                            {tag.toUpperCase()}
                                        </Tag>
                                    </>
                                ),
                            } ,
                            {
                                title: 'Date',
                                dataIndex: 'date',
                                key: 'dateDen',
                            }
                        ]} dataSource={this.state.denied} />
                        </div>}

            </div>)
    }

}
export default MyAgreements;
