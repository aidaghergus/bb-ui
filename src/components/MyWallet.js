import React, { Component } from "react";
import getWeb3 from "../web3/getWeb3";
import SwapSign from "../contracts/SwapSign.json";
import { Avatar, Card } from "antd";
import "./MyWallet.less";

import { Descriptions } from "antd";
const { Meta } = Card;

class MyWallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasData: false,
      web3: null,
      accounts: null,
      user: null,
      balance: null,
      networkId: null,
      networkType: null,
    };
  }

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();

      window.web3 = web3;
      window.user = (await web3.eth.getAccounts())[0];
      const balanceInWei = await web3.eth.getBalance(window.user);
      var balance = web3.utils.fromWei(balanceInWei, "ether");
      const deployedNetwork = SwapSign.networks[networkId];
      const instance = new web3.eth.Contract(
          SwapSign.abi,
        deployedNetwork && deployedNetwork.address
      );

      const networkType = await web3.eth.net.getNetworkType();

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({
        hasData: true,
        web3: web3,
        accounts: accounts,
        simplStorContract: instance,
        user: window.user,
        balance: balance,
        networkId: networkId,
        networkType: networkType,
      });
    } catch (error) {
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
  };

  render() {
    return (
      <div>
        {this.props.hasData ? (
          <Card className={"user-card"}>

            <Meta
              title={this.props.user}
              description={
                <div>
                  Balance: {this.props.balance} <br />
                  Network Id: {this.props.networkId} <br />
                  Network Type: {this.props.networkType} <br />
                </div>
              }
            />
          </Card>
        ) : (
          <div>No connection to Metamask</div>
        )}
      </div>
    );
  }
}

export default MyWallet;
