import React, {Component} from 'react';
import './Login.css'
import {EventEmitter} from 'fbemitter'
import UserStore from '../../apiserver/UserStore';
import {Form, Input, Button, Row, Col, Modal,} from 'antd';
import Header from "../../layout/Header";
import SignUpModal from "./SignUpModal";



const ee = new EventEmitter()
const userStore = new UserStore(ee);
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 10,
        span: 4,
    },
};


class Login extends Component{

    formRef = React.createRef();

    onFinish = values => {
        this.setState({
            username:values.username,
            password:values.password
        });
        this.logIn();
    };

    onReset = () => {
        this.formRef.current.resetFields();
    };


    constructor(props) {
        super(props);
        this.state = {
            username:"",
            password:"",
            loginerror:false
        };

        this.logIn=this.logIn.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
    }

    logIn= () =>{
        let pbody={
            username: this.state.username,
            password:this.state.password
        };
        console.log('button clicked');
        userStore.postLogin(pbody);
    }

    componentDidMount(){
        ee.addListener('LOGIN_LOAD', () => {
            let credentials=JSON.stringify(userStore.credentials);
            localStorage.setItem("Credentials",credentials);
            window.location.href="/barterplace"

          });

        ee.addListener('LOGIN_FAILED', () => {
            this.setState({
                loginerror:true
            })
        });

          ee.addListener('SIGNUP_LOAD', () => {
            let pbody={
                username: this.state.username,
                password:this.state.password
            };
            userStore.postLogin(pbody);
          })

    }

    handleInputChange(e){
        let name=e.target.name;
        let value=e.target.value;
        this.setState({
            [name]:value
        })

    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = values => {
        let pbody={
            role:"user",
            firstName:values.firstname,
            lastName:values.lastname,
            email:values.email,
            phone:values.phone,
            gender:'N',
            username:values.username,
            password:values.password,
        };
        userStore.postSignUp(pbody);

        this.setState({
            username: pbody.username,
            password: pbody.password,
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };


    render(){
        return (
            <Row style={{padding:"100px", alignItems:"center", textAlign:"center"}}>
                <Col span={10}>
                    <img src={"https://i.ibb.co/n8ktrsV/logo2.png"}/>
                </Col>
                <Col span={14}>
                <Form             layout="vertical" style={{maxWidth:"400px"}}
                                  ref={this.formRef} name="control-ref" onFinish={this.onFinish}>
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    {this.state.loginerror && <p>Invalid credentials</p>}

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>

                    </Form.Item>

                    <Button type="link" onClick={this.showModal}>
                        or sign up if you don't have an account
                    </Button>
                </Form>
                   <SignUpModal visible={this.state.visible} onCancel={this.handleCancel} onCreate={this.handleOk}/>
                </Col>
            </Row>
        )
    }
}

export default Login;
