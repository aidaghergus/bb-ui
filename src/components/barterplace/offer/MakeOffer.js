import React, {Component} from 'react';
import {Input, PageHeader, Form, Select, Button, notification, Spin, Result, Alert} from "antd";
import {EventEmitter} from "fbemitter";
import ItemStore from "../../../apiserver/ItemStore";
import getWeb3 from "../../../web3/getWeb3";
import SwapSign from "../../../contracts/SwapSign";
import OfferStore from "../../../apiserver/OfferStore";
import AgreementStore from "../../../apiserver/AgreementStore";
import AddressStore from "../../../apiserver/AddressStore";

import { SyncOutlined} from '@ant-design/icons';


import qs from "query-string";


const { Option } = Select;

const ee = new EventEmitter();
const itemStore = new ItemStore(ee);
const offerStore = new OfferStore(ee);
const agreementStore = new AgreementStore(ee);
const addressStore = new AddressStore(ee);

class MakeOffer extends Component{

    formRef = React.createRef();

    constructor(props){
        super(props);
        this.state={
            loading: false,
            itemReq:null,
            token: null,
            user:null,
            myProducts: null,
            hasData: false,
            web3: null,
            accounts: null,
            swapSign: null,
            userAcc: null,
            balance: null,
            networkId: null,
            networkType: null,
        }

    }

    getDataFromRequestParams = () => {
        let requestParams = qs.parse(this.props.location.search);
        requestParams.id &&
        itemStore.getItem(requestParams.id);
    };

    componentDidMount = async () => {
        this.getDataFromRequestParams();
        ee.addListener('ITEM_ID_LOAD', ()=> {
            console.log(itemStore.content);
            this.setState({
                itemReq: itemStore.content
            })
        });
        let credentials;
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            credentials=JSON.parse(serializedCredentials);
            this.setState((()=>({
                token: credentials.token,
                user:credentials.user
            })),()=>itemStore.getAvailableItemsOfUser(this.state.user.id));
        }

        ee.addListener('USER_AVAILAVLE_ITEMS_LOAD', ()=>{
            this.setState({
                myProducts: itemStore.content
            })
        });

        try {
            // Get network provider and web3 instance.
            const web3 = await getWeb3();

            // Use web3 to get the user's accounts.
            const accounts = await web3.eth.getAccounts();

            // Get the contract instance.
            const networkId = await web3.eth.net.getId();

            window.web3 = web3;
            window.user = (await web3.eth.getAccounts())[0];
            const balanceInWei = await web3.eth.getBalance(window.user);
            var balance = web3.utils.fromWei(balanceInWei, "ether");
            const deployedNetwork = SwapSign.networks[networkId];
            const instance = new web3.eth.Contract(
                SwapSign.abi,
                deployedNetwork && deployedNetwork.address
            );

            const networkType = await web3.eth.net.getNetworkType();

            // Set web3, accounts, and contract to the state, and then proceed with an
            // example of interacting with the contract's methods.
            this.setState({
                hasData: true,
                web3: web3,
                accounts: accounts,
                swapSign: instance,
                userAcc: window.user,
                balance: balance,
                networkId: networkId,
                networkType: networkType,
            });


        } catch (error) {
            alert(
                `Failed to load web3, accounts, or contract. Check console for details.`
            );
            console.error(error);
        }


    };

    onBack= () => {
        this.props.history.goBack();
    };

    handleCreate = (values) => {
        if(!this.state.web3 || !this.state.accounts || !this.state.swapSign){
            notification.error({message:"You need to connect your Etherum Account in order to make an offer"});
        }
        else{
            this.setState({
                loading:true
            });
            let obj={
                userId:this.state.user.id,
                itemId: values.product,
                offerMessage:values.message
            };

            offerStore.postOffer(this.state.token,obj);
            ee.addListener('OFFER_ADDED', ()=>{
                if(!offerStore.state.content.data) {
                    notification.warning({message: "Can't offer a product you've already offered"});
                    this.setState({
                        loading: false
                    });
                }
                else{
                    let solicitorOfferId=offerStore.state.content.data.id;
                    let soliciteeOfer={
                        userId:this.state.itemReq.userId,
                        itemId: this.state.itemReq.id,
                        offerMessage: "Solicitee - autmoatically created"
                    };
                    console.log(soliciteeOfer);
                    offerStore.postSoliciteeOffer(this.state.token,soliciteeOfer);
                    ee.addListener('SOLICITEE_OFFER_ADDED', ()=> {
                        let soliciteeOfferId=offerStore.state.content.data.id;
                        let agreement={
                            solicitorOfferID:solicitorOfferId,
                            soliciteeOfferId: soliciteeOfferId
                        };
                        agreementStore.postAgreement(this.state.token, agreement);
                        ee.addListener('ADD_AGREEMENT', () =>{

                            if(!agreementStore.state.content.data) {
                                notification.warning({message: "Something went wrong when adding the agreement"});
                                this.setState({
                                    loading: false
                                });
                            }
                            else{
                                let agreementId=agreementStore.state.content.data.id;
                                let address={
                                    address:this.state.userAcc
                                };
                                addressStore.postAddress(this.state.token,address.toString());
                                ee.addListener('ADD_ADDRESS', () => {
                                    this.state.swapSign.methods.addAgreement(`0x${agreementId.toString(16)}`).send({from:this.state.userAcc})
                                        .then( (res) => {
                                            console.log("Succes");
                                            this.setState({
                                                loading:false
                                            }, ()=>{notification.success({message:"Offer added"});
                                                this.props.history.push('/barterplace')});
                                        });

                                })
                            }
                        })
                    });


                }
            });


            //post offer solictor & solicitee return ids
            //post agreement with ids
            //addDocument in contract
            //add address to db

            //this.state.swapSign.addDocument(agreementId)

        }



    };

    render() {
        return(
            <div style={{margin:"0 100px 100px 100px", padding:"60px 100px 100px 100px"}}>

                <PageHeader
                    className="make-offer-page-header"
                    onBack={() => this.onBack()}
                    title={"Make offer"}
                />

                {this.state.itemReq && <Result
                    icon={<SyncOutlined />}
                    title={`You are about to sign an offer for ${this.state.itemReq.name}`}
                    extra={`Be sure you have Metamask enabeled`}
                />}

                {(this.state.myProducts && this.state.myProducts.length!==0 )? <div>
                    <Spin tip="Making the offer..." spinning={this.state.loading}>
                    <Form
                        ref={this.formRef}
                        layout="horizontal"
                        name="create_offer_form"
                        onFinish={this.handleCreate}
                        labelCol={{span: 4, offset: 0}}
                        initialValues={{
                        }}
                    >
                        <Form.Item
                            name="product"
                            label="Your offer"
                            rules={[
                                {
                                    required: true,
                                    message:
                                        "Please input a product you want to offer",
                                },
                            ]}
                        >
                            <Select
                                name="product"
                                placeholder="Select one of your products"
                            >
                                {this.state.myProducts.map((product, idx)=>{
                                    return <Option key={idx} value={product.id}>{product.name}</Option>
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="message"
                            label="Your message"
                        >
                            <Input maxLength={100}/>
                        </Form.Item>

                        <Form.Item   wrapperCol={{
                            offset: 10,
                            span: 16,
                        }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            <Button htmlType="button" onClick={this.onReset}>
                                Cancel
                            </Button>
                        </Form.Item>
                    </Form>
                    </Spin>
                </div>:<Alert style={{marginLeft:"30px"}} message="No available products to trade"
                              description="You need an available item in order to make an offer"
                              type="warning"
                              showIcon/>}

            </div>
        )
    }
}

export default MakeOffer;
