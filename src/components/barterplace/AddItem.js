import React, {Component} from 'react';

import {Form, Input, Upload, message, Button, notification, PageHeader} from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import ItemStore from "../../apiserver/ItemStore";
import {EventEmitter} from 'fbemitter'

const { Dragger } = Upload;

var publicId;

const ee = new EventEmitter();
const itemStore = new ItemStore(ee);


class AddItem extends Component{

     propsDragger = {
        name: 'file',
        multiple: true,
        action: 'https://api.cloudinary.com/v1_1/dqeihduus/upload',
        data: {  "upload_preset":'wrcchjt4' },
        onChange: (info) => this.onChange(info),
    };

    formRef = React.createRef();

    onChange = (info) => {
        const { status } = info.file;
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    };

    onReset = () => {
        this.formRef.current.resetFields();
    };

    constructor(props) {
        super(props);
        this.state={
            token:null
        }

    }

    componentDidMount() {
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.setState({
                token:credentials.token
            })
        }

        ee.addListener('ITEM_ADDED', ()=>{
            notification.success({message:"Item added"});
            this.props.history.push('/barterplace');

        })
    }

    onFinish = values => {

        let imgIds="";
        values.image.fileList.forEach(file=>{
            imgIds=imgIds.concat(file.response.public_id,"&");
        });
        imgIds=imgIds.substring(0,imgIds.length-1);
        let item={
            name:values.name,
            description: values.description,
            photoUrl: imgIds,

        };
        itemStore.postItem(this.state.token,item);
    };

    onBack= () => {
        this.props.history.push("/barterplace")
    };


    render() {
        return (
            <div style={{margin:"0 100px 100px 100px", padding:"60px 100px 100px 100px"}}>

                <PageHeader
                    className="add-item-page-header"
                    onBack={() => this.onBack()}
                    title={"Add item"}
                />
                <Form  ref={this.formRef} name="control-ref" onFinish={this.onFinish}>
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="description"
                        label="Description"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="image"
                        label="Image"
                    >
                        <Dragger {...this.propsDragger}>
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                                Upload your item images
                            </p>
                        </Dragger>
                    </Form.Item>

                    <Form.Item   wrapperCol={{
                        offset: 10,
                        span: 16,
                    }}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                        <Button htmlType="button" onClick={this.onReset}>
                            Cancel
                        </Button>
                    </Form.Item>

                </Form>

            </div>
        );
    }

}

export default AddItem;
