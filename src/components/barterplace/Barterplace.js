import React, {Component} from 'react'
import {Avatar, List, Layout, Button, Row, Col, Input, Alert, Menu, Switch} from 'antd';
import { MessageOutlined, LikeOutlined, StarOutlined,FormOutlined, FilterOutlined , SortAscendingOutlined} from '@ant-design/icons';
import {Link} from "react-router-dom";
import {EventEmitter} from "fbemitter";
import ItemStore from "../../apiserver/ItemStore";
import AgreementStore from "../../apiserver/AgreementStore";

const listData = [];
for (let i = 0; i < 23; i++) {
    listData.push({
        href: 'https://ant.design',
        title: `Jungle book`,
        avatar: 'AG',
        description:
            'added on 12.05.2020',
        content:
            'The book is in a very good state.',
    });
}

const { Search } = Input;

const ee = new EventEmitter();
const itemStore = new ItemStore(ee);
const agreementStore = new AgreementStore(ee);


class Barterplace extends Component{
    constructor(props){
        super(props);
        this.state={
            items:[],
            filteredItems:[],
            pendingAgreements:[],
            token: null
        }
    }

    componentDidMount() {
        let credentials;
        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            credentials=JSON.parse(serializedCredentials);
            agreementStore.getPendingAgreements(credentials.token);
        }
        itemStore.getAllItems()
        ee.addListener('ITEMS_LOAD', ()=>{
            this.setState({
                items: itemStore.content,
                filteredItems: itemStore.content
            })
        });

        ee.addListener('PENDING_AGREEMENTS_LOADED', ()=>{
            this.setState({
                pendingAgreements: agreementStore.content,
            })
        })

    }

    handleOnlyAvailable(checked){
        console.log(checked);
        let items=[...this.state.items];
        if(checked){
            this.setState({
                filteredItems:items.filter((el=>{
                    if(el.status==='AVAILABLE')
                        return el;
                }))
            })
        }
        else{
            this.setState({
                filteredItems:items
            })
        }

    }

    handleSearch= (value)=>{

        let items=[...this.state.items];
        this.setState({
            filteredItems:items.filter((el=>{
                if(el.name.toLowerCase().includes(value))
                    return el;
            }))
        })

    }



    render() {
        const IconText = ({ icon, text }) => (
            <div>
                {React.createElement(icon)}
                {text}
            </div>
        );

        return (
            <div style={{margin:"0px 100px 100px 100px", padding: "60px"}}>
                {this.state.pendingAgreements.length>0 && <Alert message="You have pending transactions. Go to Profile->Pending transactions in order to review them." banner />}
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 3,
                    }}
                    dataSource={this.state.filteredItems}
                    header={<div>
                        <Row gutter={{ xs: 8, sm: 16, md: 24 }}>
                            <Col span={16}>
                                <Search
                                    placeholder="Search item..."
                                    onSearch={value => this.handleSearch(value)}
                                />
                            </Col>
                            <Col span={8} gutter={10}>

                                <Button>
                                View only available items &nbsp;<Switch onChange={(checked,e)=>this.handleOnlyAvailable(checked)} />
                                </Button>

                                <Button
                                    icon={<FormOutlined />}

                                >
                                    <Link to={"/add-item"}>Post item</Link>
                                </Button>
                            </Col>
                        </Row>
                    </div>
                    }
                    renderItem={item => (
                        <List.Item
                            key={item.title}
                            actions={[
                                <IconText icon={MessageOutlined} text=" 0" key="list-vertical-message" />,
                                <Button type={"link"}  onClick={() =>{
                                    window.location.href=`/make-offer?id=${item.id}`
                                }}>Make offer</Button>
                            ]}
                            extra={
                                <img
                                    height={200}
                                    width={150}
                                    alt="logo"
                                    src={`https://res.cloudinary.com/dqeihduus/image/upload/v1593014599/${item.photoUrl.split("&")[0]}`}
                                />
                            }
                        >
                            <List.Item.Meta
                                avatar={<Avatar className="user-avatar user-avatar-mini" syle={{backgroundColor:"#add8e6"}}>{item.initials}</Avatar>}
                                title={<Link  to={{
                                    pathname: "/item",
                                    search: `?id=${item.id}`,
                                    state: { item: item }
                                }}>{item.name}</Link>}
                                description={item.addDate}
                            />
                            {item.description}
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default Barterplace;
