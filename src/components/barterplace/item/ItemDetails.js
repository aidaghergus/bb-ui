import React, {Component} from 'react'
import {Avatar, Button, Carousel, Col, Comment, PageHeader, Row, Tag, Tooltip} from "antd";
import { Typography } from 'antd';
import { Descriptions } from 'antd';
import {EventEmitter} from "fbemitter";
import UserStore from "../../../apiserver/UserStore";
import {Link} from "react-router-dom";

const { Title } = Typography;

const ee = new EventEmitter();
const userStore = new UserStore(ee);

class ItemDetails extends Component{

    constructor(props){
        super(props);
        this.state={
            user:null
        }
    }

    componentDidMount() {
        userStore.findById(this.props.location.state.item.userId);
        ee.addListener('USER_LOAD', ()=>{
            this.setState({
                user: userStore.content
            })
        })

    }

    onChange(a, b, c) {
        console.log(a, b, c);
    }

    onBack= () => {
        this.props.history.goBack();
    };

    colorReturn(status){
        if(status==="AVAILABLE")
            return "cyan";
        if(status==="WAITING")
            return "yellow";
        if(status==="TRADED")
            return "red";
    }

    render() {
        return(
            <div style={{paddingTop:"60px", paddingLeft:"50px",paddingRight:"50px"}}>
                <PageHeader
                    className="site-page-header"
                    onBack={() => this.onBack()}
                    title={this.props.location.state.item.name}
                    subTitle={`added by ${this.state.user&&this.state.user.username}`}
                />
                <Row style={{padding:"20px", backgroundColor:"#f8f9fa"}}>
                    <Col span={12}>
                        <Carousel afterChange={this.onChange} style={{width:"600px", height:"400px"}}>
                            {this.props.location.state.item.photoUrl.split("&").map((photo,idx) => {
                                return <div  key={idx}><img
                                    alt="logo"
                                    width="600px"
                                    height="400px"
                                    src={`https://res.cloudinary.com/dqeihduus/image/upload/v1593014599/${photo}`}
                                />
                                </div>
                            })}

                        </Carousel>
                    </Col>
                    <Col span={12} style={{padding:"10px"}}>

                        <Tag color={this.colorReturn(this.props.location.state.item.status)} style={{marginBottom:"5px"}}>{this.props.location.state.item.status}</Tag>

                        <Title level={4}>User Details</Title>

                        {this.state.user && <Descriptions >
                            <Descriptions.Item label="Username">{this.state.user.username}</Descriptions.Item>
                            <Descriptions.Item label="First name">{this.state.user.firstName}</Descriptions.Item>
                            <Descriptions.Item label="Last name">{this.state.user.lastName}</Descriptions.Item>
                            <Descriptions.Item label="Telephone">{this.state.user.phone}</Descriptions.Item>
                        </Descriptions> }

                        <Title level={4}>Description</Title>
                        {this.props.location.state.item.description}

                        <Title level={4}>Date added</Title>

                        {this.props.location.state.item.addDate}
                        <br/>
                        <Link to={{pathname: "/make-offer",
                            search: `?id=${this.props.location.state.item.id}`,
                            state: { item: this.props.location.state.item }}}><Button type={"primary"} style={{marginTop:"20px"}}>Make offer</Button></Link>
                    </Col>
                </Row>
                <Row style={{padding:"20px", alignItems:"center"}}>
                    <Comment

                        author={<a>Han Solo</a>}
                        avatar={
                            <Avatar
                                src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                                alt="Han Solo"
                            />
                        }
                        content={
                            <p>
                                We supply a series of design principles, practical patterns and high quality design
                                resources (Sketch and Axure), to help people create their product prototypes beautifully
                                and efficiently.
                            </p>
                        }
                        datetime={
                            <Tooltip title={"tooltip"}>
                                <span>12.10.2002</span>
                            </Tooltip>
                        }
                    />
                </Row>

            </div>
        )
    }
}

export default ItemDetails;
