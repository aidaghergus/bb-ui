import React from "react";
import Header from "./layout/Header";
import Routes from "./routes/Routes";

const App = () => (
  <div>
    <Header />
    <Routes />
  </div>
);

export default App;
