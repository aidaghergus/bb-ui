import axios from 'axios';
const SERVER = 'http://localhost:8080/api/address';

class AddressStore{
    constructor(ee){
        this.ee=ee;
        this.state={
            content: [],
            token: null

        };

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }
    }

    postAddress(token, address){
        axios({
            method:"post",
            url:SERVER,
            headers:{
                "Authorization":token
            },
            data:{"address":address}
        }).then((response) => {
            this.state.content=response;
            this.ee.emit('ADD_ADDRESS')
        })
            .catch((error) => console.warn(error))
    }
}

export default AddressStore;
