import axios from 'axios';
const SERVER = 'http://localhost:8080/api'

class UserStore{

    constructor(ee){
        this.ee=ee;
        this.state={
            credentials: null

        }
    }

    findById(id){
        axios(`${SERVER}/users/${id}`)
            .then((response) => {
                this.content = response.data
                this.ee.emit('USER_LOAD')
            })
            .catch((error) => console.warn(error))
    }

    validateToken(token){
        axios({
            method:"get",
            url:SERVER+"/validate",
            headers:{
                "Authorization":token
            }
        }).then((response) => {
            this.ee.emit('TOKEN_VALIDATED')
        })
            .catch((error) =>{ console.warn(error); this.ee.emit('TOKEN_INVALIDATED')})
    }

    postLogin(user){
        axios.post(SERVER+'/login',user)
          .then((response) => {
            this.credentials = response.data
            this.ee.emit('LOGIN_LOAD')
          })
          .catch((error) => {console.warn(error);this.ee.emit('LOGIN_FAILED')})
      }

    postSignUp(user){
        axios.post(SERVER+'/signup',user)
          .then((response) => {
            this.ee.emit('SIGNUP_LOAD')
          })
          .catch((error) => console.warn(error))
    }


}

export default UserStore;
