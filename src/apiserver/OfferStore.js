import axios from 'axios';
const SERVER = 'http://localhost:8080/api/offer';

class OfferStore{
    constructor(ee){
        this.ee=ee;
        this.state={
            content: [],
            token: null

        };

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }
    }

    postOffer(token, offer){

            axios({
                method:"post",
                url:SERVER,
                headers:{
                    "Authorization":token
                },
                data:offer
            }).then((response) => {
                this.state.content=response;
                this.ee.emit('OFFER_ADDED')
            })
                .catch((error) => console.warn(error))
    }

    postSoliciteeOffer(token, offer){
        offer.message="Solicitee - autmoatically created";
        axios({
            method:"post",
            url:SERVER+"/solicitee",
            headers:{
                "Authorization":token
            },
            data:offer
        }).then((response) => {
            this.state.content=response;
            this.ee.emit('SOLICITEE_OFFER_ADDED')
        })
            .catch((error) => console.warn(error))
    }
}

export default OfferStore;
