import axios from 'axios';
const SERVER = 'http://localhost:8080/api/item';

class ItemStore{
    constructor(ee){
        this.ee=ee;
        this.state={
            content: [],
            token: null

        };

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }
    }

    getAllItems(){
        axios(SERVER)
            .then((response) => {
                this.content = response.data
                this.ee.emit('ITEMS_LOAD')
            })
            .catch((error) => console.warn(error))
    }

    getItemsOfUser(userId){
        axios(`${SERVER}/user/all/${userId}`)
            .then((response) => {
                this.content = response.data;
                this.ee.emit('USER_ITEMS_LOAD')
            })
            .catch((error) => console.warn(error))
    }

    getAvailableItemsOfUser(userId){
        axios(`${SERVER}/user/available/${userId}`)
            .then((response) => {
                this.content = response.data;
                this.ee.emit('USER_AVAILAVLE_ITEMS_LOAD')
            })
            .catch((error) => console.warn(error))
    }

    getItem(itemId){
        axios({
            method:"get",
            url:`${SERVER}/id/${itemId}`,
        }).then((response) => {
            this.content = response.data;
            this.ee.emit('ITEM_ID_LOAD')
        })
            .catch((error) => console.warn(error))
    }

    postItem(token,item){
        axios({
            method:"post",
            url:SERVER,
            headers:{
                "Authorization":token
            },
            data:item
        }).then((response) => {
            this.ee.emit('ITEM_ADDED')
        })
            .catch((error) => console.warn(error))
    }
}

export  default ItemStore;
