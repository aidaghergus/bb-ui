import axios from 'axios';
const SERVER = 'http://localhost:8080/api/agreement';

class AgreementStore {
    constructor(ee){
        this.ee=ee;
        this.state={
            content: [],
            token: null

        };

        let serializedCredentials=localStorage.getItem("Credentials");
        if(serializedCredentials!=null){
            let credentials=JSON.parse(serializedCredentials);
            this.state.token=credentials.token;
        }
    }

    postAgreement(token, agreement){
        axios({
            method:"post",
            url:SERVER,
            headers:{
                "Authorization":token
            },
            data:agreement
        }).then((response) => {
            this.state.content=response;
            this.ee.emit('ADD_AGREEMENT')
        })
            .catch((error) => console.warn(error))
    }

    signAgreement(token, agreementId){
        axios({
            method:"post",
            url:`${SERVER}/sign/${agreementId}`,
            headers:{
                "Authorization":token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            this.ee.emit('SIGN_AGREEMENT')
        })
            .catch((error) => console.warn(error))
    }

    denyAgreement(token, agreementId){
        axios({
            method:"post",
            url:`${SERVER}/deny/${agreementId}`,
            headers:{
                "Authorization":token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            this.ee.emit('DENY_AGREEMENT')
        })
            .catch((error) => console.warn(error))
    }

    getPendingAgreements(token){
        axios(
        {
            method:"get",
                url:`${SERVER}/pending`,
            headers: {
                "Authorization": token
            }
        })
            .then((response) => {
                this.content = response.data
                this.ee.emit('PENDING_AGREEMENTS_LOADED')
            })
            .catch((error) => console.warn(error))
    }

    getWaitingAgreements(token){
        axios(
            {
                method:"get",
                url:`${SERVER}/waiting`,
                headers: {
                    "Authorization": token
                }
            })
            .then((response) => {
                this.content = response.data
                this.ee.emit('WAITING_AGREEMENTS_LOADED')
            })
            .catch((error) => console.warn(error))
    }

    getAcceptedAgreements(token){
        axios(
            {
                method:"get",
                url:`${SERVER}/accepted`,
                headers: {
                    "Authorization": token
                }
            })
            .then((response) => {
                this.content = response.data
                this.ee.emit('ACCEPTED_AGREEMENTS_LOADED')
            })
            .catch((error) => console.warn(error))
    }

    getDeniedAgreements(token){
        axios(
            {
                method:"get",
                url:`${SERVER}/denied`,
                headers: {
                    "Authorization": token
                }
            })
            .then((response) => {
                this.content = response.data
                this.ee.emit('DENIED_AGREEMENTS_LOADED')
            })
            .catch((error) => console.warn(error))
    }
}

export default AgreementStore;
