import React from "react";
import { Route, Switch } from "react-router-dom";

import MyWallet from "../components/MyWallet";
import Home from "../components/Home/Home";
import Barterplace from "../components/barterplace/Barterplace";
import AddItem from "../components/barterplace/AddItem";
import Login from "../components/login/Login";
import ItemDetails from "../components/barterplace/item/ItemDetails";
import Profile from "../components/profile/Profile";
import MakeOffer from "../components/barterplace/offer/MakeOffer";
import AboutUs from "../components/about/AboutUs";

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/my-wallet" component={MyWallet} />
        <Route exact path="/barterplace" component={Barterplace}/>
        <Route exact path="/add-item" component={AddItem}/>
        <Route exact path="/item" component={ItemDetails}/>
        <Route exact path="/profile" component={Profile}/>
        <Route exact path="/about" component={AboutUs}/>
        <Route exact path="/make-offer" component={MakeOffer}/>
          <Route exact path="/login"  component={Login}/>
          <Route
          render={function () {
            return <h1>Not Found</h1>;
          }}
        />
      </Switch>
    );
  }
}

export default Routes;
